# Napisati program koji će učitati sliku, te iz te slike napraviti 
# tri slike. Prva će imati dvostruko manje vertikalnih piksela od originala 
# (redova), druga će imati dvostruko manje horizontalnih piksela od originala 
# (stupaca), a treća će imati dvostruko manje i jednih i drugih. 
# (Hint: uzimate svaki drugi piksel)
import numpy as np
import cv2
slika=cv2.imread("slike/baboon.bmp")
cv2.imshow("pr1", slika[::2,:,:])
cv2.imshow("pr2", slika[:,::2,:])
cv2.imshow("pr3", slika[::2,::2,:])
cv2.waitKey(0)
cv2.destroyAllWindows()