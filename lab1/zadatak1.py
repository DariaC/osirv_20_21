#BGR je redoslijed
#Napisati program koji će učitati sliku i spremiti
#tri odvojene slike, po jednu za svaki kanal. Nazvati ih crvena.jpg, 
#plava.jpg i zelena.jpg. (Hint: Pri snimanju jednog kanala, postaviti 
#druga dva kanala na 0)
import cv2
import numpy as np
slika=cv2.imread('slike/baboon.bmp')
crvena=slika.copy()
crvena[:,:,0:2]=0
zelena=slika.copy()
zelena[:,:,0:3:2]=0
plava=slika.copy()
plava[:,:,1:3]=0
# print(plava[0:10,0:10,0])
# print(plava[0:10,0:10,1])
# print(plava[0:10,0:10,2])
# print(crvena[0:10,0:10,0])
# print(crvena[0:10,0:10,1])
# print(crvena[0:10,0:10,2])
# print(zelena[0:10,0:10,0])
# print(zelena[0:10,0:10,1])
# print(zelena[0:10,0:10,2])
cv2.imWrite("crvena.jpg", crvena)
cv2.imWrite("zelena.jpg", zelena)
cv2.imWrite("plava.jpg", plava)

