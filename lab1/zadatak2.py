#program otvara sve slike odjednom
import cv2
import numpy as np
slika=cv2.imread('slike/baboon.bmp')
crvena=slika.copy()
crvena[:,:,0:2]=0
zelena=slika.copy()
zelena[:,:,0:3:2]=0
plava=slika.copy()
plava[:,:,1:3]=0
cv2.imshow('crvena', crvena)
cv2.imshow('zelena', zelena)
cv2.imshow('plava', plava)
cv2.waitKey(0)
cv2.destroyAllWindows()
