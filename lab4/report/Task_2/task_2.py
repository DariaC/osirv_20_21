# Perform automatic Canny edge detection on images: airplane.bmp , barbara.bmp , boats.bmp , 
# pepper.bmp .
# Write down obtained median thresholds and save result images.

import numpy as np
import argparse
import glob
import cv2
from matplotlib import pyplot as plt

def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)
        # return the edged image
        return edged

airplane = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/airplane.bmp')
barbara = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/barbara.bmp')
boats = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/boats.bmp')
pepper = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/pepper.bmp')

images = [airplane, barbara, boats, pepper]
image_names = ['airplane', 'barbara', 'boats', 'pepper']
median_lower = [134, 71, 92, 72]
median_upper = [255, 140, 183, 143]


for i in range(4):
    auto = auto_canny(images[i])
    cv2.imwrite(image_names[i] +'_canny_' + str(median_lower[i]) + '_' + str(median_upper[i]) + '.jpg', auto)
    plt.subplot(121),plt.imshow(images[i], cmap='gray')
    plt.title('Original Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(auto,  'gray')
    plt.title('Edges'), plt.xticks([]), plt.yticks([])
    plt.show()