# Try different value combinations for lower and upper thresholds and save resultant images.
# lower=0, upper=255 
# lower=128, upper=128
# lower=255, upper=255

import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/lenna.bmp')
blurred = cv2.GaussianBlur(img, (5,5), 0) # input image blurred with Gaussian 5 by 5 kernel

combinations_lower = [0, 128, 225] #lower threshold : first threshold for the hysteresis procedure
combinations_upper = [225, 128, 225] #upper threshold : second threshold for the hysteresis procedure

for i in range(3):
    edges = cv2.Canny(blurred, combinations_lower[i], combinations_upper[i])
    cv2.imwrite('cannyedge_lenna_' + str(combinations_lower[i]) + '_' + str(combinations_upper[i]) + '.jpg', edges)
    plt.subplot(121),plt.imshow(img, cmap='gray')
    plt.title('Grayscale Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(edges,cmap = 'gray')
    plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
    plt.show()