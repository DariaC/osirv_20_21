# Find lines with Hough transform on the image: chess.jpg.
# Change theta and threshold parameters to the following values:
# theta= 90, threshold = 150
# theta=180, threshold = 200
# theta= 90, threshold = 200
# theta=180, threshold = 150

import cv2
import numpy as np
import math
import copy
from matplotlib import pyplot as plt

img = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 166, 255)

thetas = [90, 180, 90, 180]
thresholds = [150, 200, 200, 150]

for i in range(4):
   img3 = copy.copy(img2) #mora se uzeti nova slika inače se nadodaju linije 
   lines= cv2.HoughLines(edges, 1, math.pi/thetas[i], thresholds[i], np.array([]), 0, 0)
   a,b,c = lines.shape
   for j in range(a):
       rho = lines[j][0][0]
       theta = lines[j][0][1]
       a = math.cos(theta)
       b = math.sin(theta)
       x0, y0 = a*rho, b*rho
       pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
       pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
       cv2.line(img3, pt1, pt2, (255, 0, 0), 2, cv2.LINE_AA)
   plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
   plt.title('Original Image'), plt.xticks([]), plt.yticks([])
   plt.subplot(122),plt.imshow(img3, 'gray')
   plt.title('Detected Lines'), plt.xticks([]), plt.yticks([])
   plt.show()   
   cv2.imwrite('HT_chess_' + str(thetas[i]) + '_' + str(thresholds[i]) + '.jpg', img3)

