# Perform Harris Corner detection on image chess.jpg .
# Change size of the neighborhood values as follows:
# blocksize=1, blocksize=3, blocksize=5

import cv2
import numpy as np
from matplotlib import pyplot as plt
import copy

img = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)

blocksizes = [1, 3, 5]

for i in range(3):
    dst = cv2.cornerHarris(gray, blocksizes[i], 3, 0.04)
    dst = cv2.dilate(dst,None)
    img2[dst>0.01*dst.max()]=[0,0,255] 
    plt.imshow(img2)
    plt.title('Detected Corners'), plt.xticks([]), plt.yticks([])
    plt.show()
    cv2.imwrite('HC_chess_' + str(blocksizes[i]) + '.jpg', img2)