# Izvještaj LV4

## Zadatak 1.

***Kod***

```python
import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/lenna.bmp')
blurred = cv2.GaussianBlur(img, (5,5), 0) # input image blurred with Gaussian 5 by 5 kernel

combinations_lower = [0, 128, 225] #lower threshold : first threshold for the hysteresis procedure
combinations_upper = [225, 128, 225] #upper threshold : second threshold for the hysteresis procedure

for i in range(3):
    edges = cv2.Canny(blurred, combinations_lower[i], combinations_upper[i])
    cv2.imwrite('cannyedge_lenna_' + str(combinations_lower[i]) + '_' + str(combinations_upper[i]) + '.jpg', edges)
    plt.subplot(121),plt.imshow(img, cmap='gray')
    plt.title('Grayscale Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(edges,cmap = 'gray')
    plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
    plt.show()
```

***Slike:***

 - Median lower = 0, Median upper = 255

![ced1im1](Task_1/cannyedge_lenna_0_225.jpg)

 - Median lower = 128, Median upper = 128

![ce1im2](Task_1/cannyedge_lenna_128_128.jpg)

 - Median lower = 255, Median upper = 255

![ced1im3](Task_1/cannyedge_lenna_225_225.jpg)

## Zadatak 2.

***Kod***

```python
import numpy as np
import argparse
import glob
import cv2
from matplotlib import pyplot as plt

def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print ("Median lower: %r." % lower)
        print ("Median upper: %r." % upper)
        # return the edged image
        return edged

airplane = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/airplane.bmp')
barbara = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/barbara.bmp')
boats = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/boats.bmp')
pepper = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/pepper.bmp')

images = [airplane, barbara, boats, pepper]
image_names = ['airplane', 'barbara', 'boats', 'pepper']
median_lower = [134, 71, 92, 72]
median_upper = [255, 140, 183, 143]


for i in range(4):
    auto = auto_canny(images[i])
    cv2.imwrite(image_names[i] +'_canny_' + str(median_lower[i]) + '_' + str(median_upper[i]) + '.jpg', auto)
    plt.subplot(121),plt.imshow(images[i], cmap='gray')
    plt.title('Original Image'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(auto,  'gray')
    plt.title('Edges'), plt.xticks([]), plt.yticks([])
    plt.show()
```
***Slike:***

 - Median lower = 134, Median upper = 255

![ced2im1](Task_2/airplane_canny_134_255.jpg)

 - Median lower = 71, Median upper = 140

![ced2im2](Task_2/barbara_canny_71_140.jpg)

 -  Median lower = 92, Median upper = 183

![ced2im3](Task_2/boats_canny_92_183.jpg)

 - Median lower = 72, Median upper = 143

![ced2im4](Task_2/pepper_canny_72_143.jpg)

## Zadatak 3.

***Kod***

```python
import cv2
import numpy as np
import math
import copy
from matplotlib import pyplot as plt

img = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gray, 166, 255)

thetas = [90, 180, 90, 180]
thresholds = [150, 200, 200, 150]

for i in range(4):
   img3 = copy.copy(img2) #mora se uzeti nova slika inače se nadodaju linije samo
   lines= cv2.HoughLines(edges, 1, math.pi/thetas[i], thresholds[i], np.array([]), 0, 0)
   a,b,c = lines.shape
   for j in range(a):
       rho = lines[j][0][0]
       theta = lines[j][0][1]
       a = math.cos(theta)
       b = math.sin(theta)
       x0, y0 = a*rho, b*rho
       pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
       pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
       cv2.line(img3, pt1, pt2, (255, 0, 0), 2, cv2.LINE_AA)
   plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
   plt.title('Original Image'), plt.xticks([]), plt.yticks([])
   plt.subplot(122),plt.imshow(img3, 'gray')
   plt.title('Detected Lines'), plt.xticks([]), plt.yticks([])
   plt.show()   
   cv2.imwrite('HT_chess_' + str(thetas[i]) + '_' + str(thresholds[i]) + '.jpg', img3)
```

***Slike:***

 - Theta = 90, Threshold = 150

![htim1](Task_3/HT_chess_90_150.jpg)

 - Theta = 180, Threshold = 200

![htim2](Task_3/HT_chess_180_200.jpg)

 - Theta = 90, Threshold = 200

![htim3](Task_3/HT_chess_90_200.jpg)

 - Theta = 180, Threshold = 150

![htim4](Task_3/HT_chess_180_150.jpg)

## Zadatak 4.

***Kod***

```python
import cv2
import numpy as np
from matplotlib import pyplot as plt
import copy

img = cv2.imread('C:/Users/daria/osirv_20_21/lab4/slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)

blocksizes = [1, 3, 5]

for i in range(3):
    dst = cv2.cornerHarris(gray, blocksizes[i], 3, 0.04)
    dst = cv2.dilate(dst,None)
    img2[dst>0.01*dst.max()]=[0,0,255] 
    plt.imshow(img2)
    plt.title('Detected Corners'), plt.xticks([]), plt.yticks([])
    plt.show()
    cv2.imwrite('HC_chess_' + str(blocksizes[i]) + '.jpg', img2)
```
***Slike:***

 - Blocksize = 1

![hcim1](Task_4/HC_chess_1.jpg)

 - Blocksize = 3

![hcim2](Task_4/HC_chess_3.jpg)

 - Blocksize = 5

![hcim3](Task_4/HC_chess_5.jpg)

## Zadatak 5.

***Kod***

```python
import numpy as np
import pandas as pd
import cv2
import matplotlib.pyplot as plt
import os

# Features Detection: 
dataset_path = '.'
img = cv2.imread(os.path.join(dataset_path, 'C:/Users/daria/osirv_20_21/lab4/slike/roma_1.jpg'))
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)  # Convert

orb = cv2.ORB_create()
key_points, description = orb.detectAndCompute(img, None)
# rich keypoints: 
img_keypoints =cv2.drawKeypoints(img,key_points,img,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
cv2.imwrite('ORB_interest_points_roma1.jpg', img_keypoints)

#Feature Extraction:
def image_detect_and_compute(detector, img_name):
    """Detect and compute intetrest points and their descriptors."""
    img = cv2.imread(os.path.join(dataset_path, img_name))
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    kp, des = detector.detectAndCompute(img, None)
    return img, kp, des

def draw_image_matches(detector, img1_name, img2_name, nmatches=20):
    """Draw ORB feature matches of the given two images."""
    img1, kp1, des1 = image_detect_and_compute(detector, img1_name)
    img2, kp2, des2 = image_detect_and_compute(detector, img2_name)

    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = bf.match(des1, des2)
    matches = sorted(matches, key = lambda x: x.distance)

    img_matches = cv2.drawMatches(img1, kp1, img2, kp2, matches[:nmatches],img2, flags=2)

    #Draw Resultant Images:
    plt.figure(1)
    plt.imshow(img_keypoints)
    plt.title('ORB Interest Point'), plt.xticks([]), plt.yticks([])
    plt.figure(2)
    plt.imshow(img_matches)
    plt.title('Detector'), plt.xticks([]), plt.yticks([])
    plt.show()
    return img_matches #dodano


orb = cv2.ORB_create()
img_matches = draw_image_matches(orb, 'C:/Users/daria/osirv_20_21/lab4/slike/roma_1.jpg','C:/Users/daria/osirv_20_21/lab4/slike/roma_2.jpg')
cv2.imwrite('Detector_roma1_roma2.jpg', img_matches)
```

***Slike:***

 - ORB Interest Point

[!im1](Task_5/ORB_interest_points_roma1.jpg)

 - Detector

[!im2](Task_5/Detector_roma1_roma2.jpg)