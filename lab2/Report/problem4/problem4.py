# Create a program which will load all images from images directory
# perform the threshold operation with the parameter values of 63, 127, 191
# threshold operation should set all pixels with values <= threshold to 0 and leave
# the values >= threshold as they are.
# save the images to problem4/imagename_parametervalue_thresh.png
import numpy as np
import cv2
import os

images=[]

def threshold(img,parameter):
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if(img[i,j] <= parameter):
                img[i,j]=0
    return img

def load_images_from_folder(folder, x):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename), 0) # uzela greyscale jer drugčije nije radilo
        if img is not None:
            images.append(img)
            cv2.imwrite(filename+'_' + str(x) +'_tresh.png', threshold(img, x))
    return images

folder='C:/Users/daria/osirv_20_21/lab2/Report/problem4/slike'

images=load_images_from_folder(folder, 63)
images2=load_images_from_folder(folder, 127)
images3=load_images_from_folder(folder, 191)