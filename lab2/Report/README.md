# Izvještaj LV2

## Problem 1. 

***Kod***

```python
import numpy as np
import cv2

img1 = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/baboon.bmp')
img2 = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/pepper.bmp')
img3 = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/goldhill.bmp')
print(img1.shape, img2.shape, img3.shape) # gledamo koje su dimenzije kako bi odredila kako odrezat sliku
final_img=np.hstack((img1,img2[0:480,0:500,:],img3[0:480, 0:500,:]))
cv2.imshow('spojeno', final_img)
cv2.imwrite("spojena slika.jpg", final_img)
# definicija funkcije koja pomoću hstack i vstack pravi sliku od granica
# koje predstavaljaju druge matrice popunjene 0 (crna boja) i slike predane funkciji

def make_border(image, top, bottom, left, right):
    bottom_fin=np.zeros((bottom, image.shape[1], 3), dtype="uint8")
    top_fin=np.zeros((top, image.shape[1], 3), dtype="uint8")
    left_fin=np.zeros((image.shape[0] + bottom + top, left, 3), dtype="uint8")
    right_fin=np.zeros((image.shape[0] + bottom + top, right, 3), dtype="uint8")
    image=np.vstack((top_fin, image, bottom_fin))
    image=np.hstack((left_fin, image, right_fin))
    return image

img4=make_border(img1, 20, 20, 10, 10) # provjera funkcije na običnoj slici
img5=make_border(final_img, 5, 5, 5, 5) # provjera funkcije na napravljenoj slici

cv2.imshow('dodan okvir', img4)
cv2.imshow('spojeno + dodatan okvir', img5)
cv2.waitKey(0)
cv2.destroyAllWindows()
```
#### Slike

![p1s1](problem1/spojena slika.jpg)

## Problem 2.

***Kod***

```python
import numpy as np
import cv2

# definicja konvolucije kopirana iz predloška
def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 

sharpen_kernel=np.array([[0,-1, 0],[ -1, 5, -1],[0, -1, 0]])

img=cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/baboon.bmp', 0) # greyscale

img_fin=convolve(img, sharpen_kernel)

cv2.imwrite('sharpen.jpg', img_fin)

cv2.imshow('rezultat', img_fin)
cv2.waitKey(0)
cv2.destroyAllWindows()
```
#### Slike

![p2s1](problem2/sharpen.jpg)

## Problem 3.

***Kod***

```python
import numpy as np
import cv2

def invertfunc(img):
    img=(255-img)
    return img


img1=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/airplane.bmp",0)
img2=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/baboon.bmp",0)
img3=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/barbara.bmp",0)
img4=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/barbara.pgm",0)
img5=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/boats.bmp",0)
img6=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/BoatsColor.bmp",0)
img7=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/goldhill.bmp",0)
img8=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/hp_logo.jpg",0)
img9=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/lenna.bmp",0)
img10=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/pepper.bmp",0)

img1=invertfunc(img1)
img2=invertfunc(img2)
img3=invertfunc(img3)
img4=invertfunc(img4)
img5=invertfunc(img5)
img6=invertfunc(img6)
img7=invertfunc(img7)
img8=invertfunc(img8)
img9=invertfunc(img9)
img10=invertfunc(img10)

cv2.imwrite("airplane_invert.jpg", img1)
cv2.imwrite("baboon_invert.jpg", img2)
cv2.imwrite("barbara_invert.jpg", img3)
cv2.imwrite("barbara2_invert.jpg", img4)
cv2.imwrite("boats_invert.jpg", img5)
cv2.imwrite("BoatsColor_invert.jpg", img6)
cv2.imwrite("goldgill_invert.jpg", img7)
cv2.imwrite("hp_logo_invert.jpg", img8)
cv2.imwrite("lenna_invert.jpg", img9)
cv2.imwrite("pepper_invert.jpg", img10)
```
#### Slike

![p3s1](problem3/airplane_invert.jpg)

![p3s2](problem3/baboon_invert.jpg)

![p3s3](problem3/barbara_invert.jpg)

![p3s4](problem3/boats_invert.jpg)

![p3s5](problem3/BoatsColor_invert.jpg)

![p3s6](problem3/goldgill_invert.jpg)

![p3s7](problem3/hp_logo_invert.jpg)

![p3s8](problem3/lenna_invert.jpg)

![p3s9](problem3/pepper_invert.jpg)

## Problem 4.

***Kod***

```python
import numpy as np
import cv2
import os

images=[]

def threshold(img,parameter):
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if(img[i,j] <= parameter):
                img[i,j]=0
    return img

def load_images_from_folder(folder,x ):
    images = []
    for filename in os.listdir(folder):
        img = cv2.imread(os.path.join(folder,filename), 0) # uzela greyscale jer drugčije nije radilo
        if img is not None:
            images.append(img)
            cv2.imwrite(filename+'_'+str(x)+'_tresh.png', threshold(img, x))
    return images

folder='C:/Users/daria/osirv_20_21/lab2/Report/problem4/slike'

images=load_images_from_folder(folder, 63)
images2=load_images_from_folder(folder, 127)
images3=load_images_from_folder(folder, 191)
```
#### Slike

![p4s1](problem4/airplane.bmp_63_tresh.png)

![p4s2](problem4/airplane.bmp_127_tresh.png)

![p4s3](problem4/airplane.bmp_191_tresh.png)

Zbog količine slika, prikazuje se samo airplane. Ostatak se nalazi u mapi "problem4".

## Problem 5.

***Kod***

```python
from numpy import *
import cv2

img = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/BoatsColor.bmp', 0) # ne prihvaća CV_LOAD_IMAGE_GRAYSCALE
img = img.astype(float32)

for q in range(1, 9):
    d = (2 ** (8 - q))
    img_final = img.copy()
    for i in range(0, img_final.shape[0]):
        for j in range(0, img_final.shape[1]):
            img_final[i][j] = (floor(img_final[i][j] / d) + 0.5) * d # formula dana u predlošku
            
    img_final[img_final > 225] =  225 # ako je veće od 255 postaljva na 255
    
    img_final[img_final < 0] = 0 # ako je manje od 0 postavlja n a0
    
    img_final.astype(uint8)
    cv2.imwrite('boast_'+ str(q)+'.bmp', img_final) # može primiti q samo u obliku string
```
#### Slike

![p5s1](problem5/boast_1.bmp)

![p5s2](problem5/boast_2.bmp)

![p5s3](problem5/boast_3.bmp)

![p5s4](problem5/boast_4.bmp)

![p5s5](problem5/boast_5.bmp)

![p5s6](problem5/boast_6.bmp)

![p5s7](problem5/boast_7.bmp)

![p5s8](problem5/boast_8.bmp)

## Problem 5.

***Kod***

```python
from numpy import *
import cv2

img = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/BoatsColor.bmp', 0) 
img = img.astype(float32)

noise = random.uniform(0, 1, img.shape) 

for q in range(1, 9):
    d = (2 ** (8 - q))
    img_final = img.copy()
    for i in range(0, img_final.shape[0]):
        for j in range(0, img_final.shape[1]):
            img_final[i][j] = (floor(img_final[i][j] + noise[i][j]) + 0.5) * d # formula dana u predlošku
            
    img_final[img_final > 225] =  225 
    
    img_final[img_final < 0] = 0 
    
    img_final.astype(uint8)
    cv2.imwrite('boast_'+ str(q)+'.bmp', img_final) 
```
#### Slike 

![p6s1](problem6/boast_1.bmp)

![p6s2](problem6/boast_2.bmp)

![p6s3](problem6/boast_3.bmp)

![p6s4](problem6/boast_4.bmp)

![p6s5](problem6/boast_5.bmp)

![p6s6](problem6/boast_6.bmp)

![p6s7](problem6/boast_7.bmp)

![p6s8](problem6/boast_8.bmp)

## Problem 7.

***Kod***

```python
from numpy import *
import cv2
from math import pi

img = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/baboon.bmp') 

scaled = cv2.resize(img,None,fx=0.25, fy=0.25) # scale na četvrtinu 

final = scaled.copy()

deg=30

while deg <= 360:
    theta = (30 * pi) / 180
    M = cv2.getRotationMatrix2D((scaled.shape[1]/2, scaled.shape[0]/2), deg, 1)
    rotated = cv2.warpAffine(scaled.copy(), M, (scaled.shape[1],scaled.shape[0]))
    final = hstack((final, rotated))
    deg += 30

cv2.imwrite('konacna_slika.jpg', final)
```
#### Slike

![p7s1](problem7/konacna_slika.jpg)



