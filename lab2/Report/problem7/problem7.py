# Create a following
# image:
# by iteratively rotating baboon image for 30 degrees between iterations, and
# stacking results together. Before applying rotation to the image, scale the
# image to quarter of its width and height.

from numpy import *
import cv2
from math import pi

img = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/baboon.bmp') 

scaled = cv2.resize(img,None,fx=0.25, fy=0.25) # scale na četvrtinu 

final = scaled.copy()

deg=30

while deg <= 360:
    theta = (30 * pi) / 180
    M = cv2.getRotationMatrix2D((scaled.shape[1]/2, scaled.shape[0]/2), deg, 1)
    rotated = cv2.warpAffine(scaled.copy(), M, (scaled.shape[1],scaled.shape[0]))
    final = hstack((final, rotated))
    deg += 30

cv2.imwrite('konacna_slika.jpg', final)

