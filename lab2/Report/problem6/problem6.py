# Perform the quantisation with the added noise, according to the formula
# above. Use the same image and quantisation values as in Problem 3.
# Save the images as
# .bmp files with the filename format boats_qn.bmp,  where q is the
# numeric value of qqq used for that image.
# Comment the visual quality of quantised images with noise compared to
# quantised images in Problem 3, for all the values of qqq.
# For generation of uniform noise use np.random.uniform(0,1, shape ) numpy
# function (shape is the shape of resulting array).
# See Numpy
# documentation.
from numpy import *
import cv2

img = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/BoatsColor.bmp', 0) 
img = img.astype(float32)

noise = random.uniform(0, 1, img.shape) 

for q in range(1, 9):
    d = (2 ** (8 - q))
    img_final = img.copy()
    for i in range(0, img_final.shape[0]):
        for j in range(0, img_final.shape[1]):
            img_final[i][j] = (floor(img_final[i][j] + noise[i][j]) + 0.5) * d # formula dana u predlošku
            
    img_final[img_final > 225] =  225 
    
    img_final[img_final < 0] = 0 
    
    img_final.astype(uint8)
    cv2.imwrite('boast_'+ str(q)+'.bmp', img_final) 

# komentar na rezultate:
# za razliku od rezultatau prošlom zadatku, ovdje se sa povećanjem q ne povećava znatno
# kvaliteta dok ne dođe do max q kada slika izgleda normalno
# u usporedbi sa prošlim rezultatima ove slike su puno lošije (ako se smanji q + šum)