# Create a program which will load all images from images directory in grayscale
# perform the invert operation and
# save the images to problem3/imagename_invert.png.
import numpy as np
import cv2

def invertfunc(img):
    img=(255-img)
    return img


img1=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/airplane.bmp",0)
img2=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/baboon.bmp",0)
img3=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/barbara.bmp",0)
img4=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/barbara.pgm",0)
img5=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/boats.bmp",0)
img6=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/BoatsColor.bmp",0)
img7=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/goldhill.bmp",0)
img8=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/hp_logo.jpg",0)
img9=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/lenna.bmp",0)
img10=cv2.imread("C:/Users/daria/osirv_20_21/lab2/slike/pepper.bmp",0)

img1=invertfunc(img1)
img2=invertfunc(img2)
img3=invertfunc(img3)
img4=invertfunc(img4)
img5=invertfunc(img5)
img6=invertfunc(img6)
img7=invertfunc(img7)
img8=invertfunc(img8)
img9=invertfunc(img9)
img10=invertfunc(img10)

cv2.imwrite("airplane_invert.jpg", img1)
cv2.imwrite("baboon_invert.jpg", img2)
cv2.imwrite("barbara_invert.jpg", img3)
cv2.imwrite("barbara2_invert.jpg", img4)
cv2.imwrite("boats_invert.jpg", img5)
cv2.imwrite("BoatsColor_invert.jpg", img6)
cv2.imwrite("goldgill_invert.jpg", img7)
cv2.imwrite("hp_logo_invert.jpg", img8)
cv2.imwrite("lenna_invert.jpg", img9)
cv2.imwrite("pepper_invert.jpg", img10)

