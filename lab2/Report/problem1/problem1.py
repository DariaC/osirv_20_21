# Učitajte iz foldera images ( rootu repoa) tri različite slike, te ih spojite u jednu
# sliku jednu do druge. Prikažite i spremite tu sliku. Koristiti funkcije hstack ili
# vstack. Dozvoljeno je odsjeći dio slike kako bi svi dijelovi bili istih
# dimenzija.
# Učitajte sliku i dodajte crni okvir oko slike, ali na način da ne
# prepisujete dijelove slike, već da dodate okvir na rub slike. Kod za dodavanje
# okvira stavite u funkciju i omogućite dodavanje proizvoljne širine okvira
# (širina se predaje funkciji kao parametar).

import numpy as np
import cv2

img1 = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/baboon.bmp')
img2 = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/pepper.bmp')
img3 = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/goldhill.bmp')
print(img1.shape, img2.shape, img3.shape) # gledamo koje su dimenzije kako bi odredila kako odrezat sliku
final_img=np.hstack((img1,img2[0:480,0:500,:],img3[0:480, 0:500,:]))
cv2.imshow('spojeno', final_img)
cv2.imwrite("spojena slika.jpg", final_img)
# definicija funkcije koja pomoću hstack i vstack pravi sliku od granica
# koje predstavaljaju druge matrice popunjene 0 (crna boja) i slike predane funkciji

def make_border(image, top, bottom, left, right):
    bottom_fin=np.zeros((bottom, image.shape[1], 3), dtype="uint8")
    top_fin=np.zeros((top, image.shape[1], 3), dtype="uint8")
    left_fin=np.zeros((image.shape[0] + bottom + top, left, 3), dtype="uint8")
    right_fin=np.zeros((image.shape[0] + bottom + top, right, 3), dtype="uint8")
    image=np.vstack((top_fin, image, bottom_fin))
    image=np.hstack((left_fin, image, right_fin))
    return image

img4=make_border(img1, 20, 20, 10, 10) # provjera funkcije na običnoj slici
img5=make_border(final_img, 5, 5, 5, 5) # provjera funkcije na napravljenoj slici

cv2.imshow('dodan okvir', img4)
cv2.imshow('spojeno + dodatan okvir', img5)
cv2.waitKey(0)
cv2.destroyAllWindows()


