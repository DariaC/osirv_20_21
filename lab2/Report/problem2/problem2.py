# Izvrtite konvoluciju na slici po izboru za primjere kernela sa linka .
# Kod i rezultantne slike dodajte u report.
import numpy as np
import cv2

# definicja konvolucije kopirana iz predloška
def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1) )
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 

sharpen_kernel=np.array([[0,-1, 0],[ -1, 5, -1],[0, -1, 0]])

img=cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/baboon.bmp', 0) # greyscale

img_fin=convolve(img, sharpen_kernel)

cv2.imwrite('sharpen.jpg', img_fin)

cv2.imshow('rezultat', img_fin)
cv2.waitKey(0)
cv2.destroyAllWindows()