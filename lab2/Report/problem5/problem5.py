# Perform the quantisation on the image
# BoatsColor.bmp
# for all values of qqq in the interval  [1,8][1,8][1,8] .
# Save the images as
# .bmp files with the filename format boats_q.bmp,  where q is the
# numeric value of qqq used for that image.
# Comment the visual quality of quantised images compared to original image, for
# all  values of  qqq.

from numpy import *
import cv2

img = cv2.imread('C:/Users/daria/osirv_20_21/lab2/slike/BoatsColor.bmp', 0) # ne prihvaća CV_LOAD_IMAGE_GRAYSCALE
img = img.astype(float32)

for q in range(1, 9):
    d = (2 ** (8 - q))
    img_final = img.copy()
    for i in range(0, img_final.shape[0]):
        for j in range(0, img_final.shape[1]):
            img_final[i][j] = (floor(img_final[i][j] / d) + 0.5) * d # formula dana u predlošku
            
    img_final[img_final > 225] =  225 # ako je veće od 255 postaljva na 255
    
    img_final[img_final < 0] = 0 # ako je manje od 0 postavlja n a0
    
    img_final.astype(uint8)
    cv2.imwrite('boast_'+ str(q)+'.bmp', img_final) # može primiti q samo u obliku string
    
# komentar na slike:
# što je veći q to je slika jasnija, ima više tonova i kvalitetnija