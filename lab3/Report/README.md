# Izvještaj LV3

## Zadatak 1. - Gaussian, salt n pepper i uniform noise

***Kod***

```python
import numpy as np
import cv2
import matplotlib.pyplot as plt

img=cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/baboon.bmp',0)

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()
 
showhist(gaussian_noise(img, 0, 15))
showhist(gaussian_noise(img, 0, 25))
showhist(gaussian_noise(img, 0, 40))
showhist(salt_n_pepper_noise(img, 5))
showhist(salt_n_pepper_noise(img, 10))
showhist(salt_n_pepper_noise(img, 15))
showhist(salt_n_pepper_noise(img, 20))
showhist(uniform_noise(img, -20, 20))
showhist(uniform_noise(img, -40, 40))
showhist(uniform_noise(img, -60, 60))

cv2.imwrite("baboon_gaussian_noise_15.jpg", gaussian_noise(img, 0, 15))
cv2.imwrite("baboon_gaussian_noise_25.jpg", gaussian_noise(img, 0, 25))
cv2.imwrite("baboon_gaussian_noise_40.jpg", gaussian_noise(img, 0, 40))
cv2.imwrite("baboon_salt_n_pepper_noise_5.jpg", salt_n_pepper_noise(img, 5))
cv2.imwrite("baboon_salt_n_pepper_noise_10.jpg", salt_n_pepper_noise(img, 10))
cv2.imwrite("baboon_salt_n_pepper_noise_15.jpg", salt_n_pepper_noise(img, 15))
cv2.imwrite("baboon_salt_n_pepper_noise_20.jpg", salt_n_pepper_noise(img, 20))
cv2.imwrite("baboon_uniform_noise_20.jpg", uniform_noise(img, -20, 20))
cv2.imwrite("baboon_uniform_noise_40.jpg", uniform_noise(img, -40, 40))
cv2.imwrite("baboon_uniform_noise_60.jpg", uniform_noise(img, -60, 60))
```
#### Slike gaussian noise sa vrijednost sigma = [15, 25, 40]

![gn15](Zadatak 1/baboon_gaussian_noise_15.jpg)

![gn25](Zadatak 1/baboon_gaussian_noise_25.jpg)

![gn40](Zadatak 1/baboon_gaussian_noise_40.jpg)

#### Slike salt n pepper noise sa postotcima [5, 10, 15, 20]

![snp5](Zadatak 1/baboon_salt_n_pepper_noise_5.jpg)

![snp10](Zadatak 1/baboon_salt_n_pepper_noise_10.jpg)

![snp15](Zadatak 1/baboon_salt_n_pepper_noise_15.jpg)

![snp20](Zadatak 1/baboon_salt_n_pepper_noise_20.jpg)

#### Slike uniform noise sa vrijednostima granice [(−20,20),(−40,40),(−60,60)]

![u20](Zadatak 1/baboon_uniform_noise_20.jpg)

![u40](Zadatak 1/baboon_uniform_noise_40.jpg)

![u60](Zadatak 1/baboon_uniform_noise_60.jpg)

## Exercises 1. - Gaussian i median filter

***Kod***

```python
import numpy as np
import cv2
import matplotlib.pyplot as plt

img1=cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/boats.bmp')
img2=cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/airplane.bmp')

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

salt_n_pepper_p =[1,10]

gaussian_n=[5, 15, 35]

median=[3, 5, 7]

blur=[1, 5, 9]

for x in gaussian_n:
    img_b = gaussian_noise(img1, 0, x)
    img_a = gaussian_noise(img2, 0, x)
    for y in median:
        img_med_b= cv2.medianBlur(img_b, y)
        cv2.imwrite('boats_gaussian_' + str(x) + '_median_' + str(y) + '.jpg', img_med_b)
        img_med_a= cv2.medianBlur(img_a, y)
        cv2.imwrite('airplane_gaussian_' + str(x) + '_median_' + str(y) + '.jpg', img_med_a)
    for z in blur:
        img_blur_b = cv2.GaussianBlur(img_b, (z, z), x)
        cv2.imwrite('boats_gaussian_'+ str(x) + '_gaussian_blur_' + str(z) + '.jpg', img_blur_b)
        img_blur_a = cv2.GaussianBlur(img_a, (z, z), x)
        cv2.imwrite('airplane_gaussian_'+ str(x) + '_gaussian_blur_' + str(z) + '.jpg', img_blur_a)

for x in salt_n_pepper_p:
    img_b = salt_n_pepper_noise(img1, x)
    img_a = salt_n_pepper_noise(img2, x)
    for y in median:
        img_med_b= cv2.medianBlur(img_b, y)
        cv2.imwrite('boats_salt_n_pepper_' + str(x) + '_median_' + str(y) + '.jpg', img_med_b)
        img_med_a= cv2.medianBlur(img_a, y)
        cv2.imwrite('airplane_salt_n_pepper_' + str(x) + '_median_' + str(y) + '.jpg', img_med_a)
    for z in blur:
        img_blur_b = cv2.GaussianBlur(img_b, (z, z), x)
        cv2.imwrite('boats_salt_n_papper_'+ str(x) + '_gaussian_blur_' + str(z) + '.jpg', img_blur_b)
        img_blur_a = cv2.GaussianBlur(img_a, (z, z), x)
        cv2.imwrite('airplane_salt_n_papper_'+ str(x) + '_gaussian_blur_' + str(z) + '.jpg', img_blur_a)

def med_filter(image, radius):
    imgcopy=image
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            imgcopy[i][j]=np.median(imgcopy[i:i+radius, j:j+radius])
    return imgcopy
   
img_z4=cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/boats.bmp',0)

noise = salt_n_pepper_noise(img_z4, 10)
my_med = med_filter(img_z4, 3)
or_med = cv2.medianBlur(img_z4, 3)

cv2.imwrite('noise_test.jpg', noise)
cv2.imwrite('my_filter.jpg', my_med)
cv2.imwrite('original_filter.jpg', or_med)
```
#### Gaussian noise i median filtar

![gn5mf3](Exercises 1/airplane_gaussian_5_median_3.jpg)

![gn5mf5](Exercises 1/airplane_gaussian_5_median_5.jpg)

![gn5mf7](Exercises 1/airplane_gaussian_5_median_7.jpg)

![gn15mf3](Exercises 1/airplane_gaussian_15_median_3.jpg)

![gn15mf5](Exercises 1/airplane_gaussian_15_median_5.jpg)

![gn15mf7](Exercises 1/airplane_gaussian_15_median_7.jpg)

![gn35mf3](Exercises 1/airplane_gaussian_35_median_3.jpg)

![gn35mf5](Exercises 1/airplane_gaussian_35_median_5.jpg)

![gn35mf7](Exercises 1/airplane_gaussian_35_median_7.jpg)

U sklopu vježbe, kao što se može vidjeti iz koda, napravljeno je i za sliku boats, ali radi velikog broja slika nije uključeno u md file.

#### Salt n pepper noise i median filter

![snp1mf3](Exercises 1/airplane_salt_n_pepper_1_median_3.jpg)

![snp1mf5](Exercises 1/airplane_salt_n_pepper_1_median_5.jpg)

![snp1mf7](Exercises 1/airplane_salt_n_pepper_1_median_7.jpg)

![snp10mf3](Exercises 1/airplane_salt_n_pepper_10_median_3.jpg)

![snp10mf5](Exercises 1/airplane_salt_n_pepper_10_median_5.jpg)

![snp10mf7](Exercises 1/airplane_salt_n_pepper_10_median_7.jpg)

#### Gaussian noise i gaussian filter

![gn5gb1](Exercises 1/airplane_gaussian_5_gaussian_blur_1.jpg)

![gn5gb5](Exercises 1/airplane_gaussian_5_gaussian_blur_5.jpg)

![gn5gb9](Exercises 1/airplane_gaussian_5_gaussian_blur_9.jpg)

![gn15gb1](Exercises 1/airplane_gaussian_15_gaussian_blur_1.jpg)

![gn15gb5](Exercises 1/airplane_gaussian_15_gaussian_blur_5.jpg)

![gn15gb9](Exercises 1/airplane_gaussian_15_gaussian_blur_9.jpg)

![gn35gb1](Exercises 1/airplane_gaussian_35_gaussian_blur_1.jpg)

![gn35gb5](Exercises 1/airplane_gaussian_35_gaussian_blur_5.jpg)

![gn35gb9](Exercises 1/airplane_gaussian_35_gaussian_blur_9.jpg)

#### Salt n pepper noise i gaussian filter

![snp1gb1](Exercises 1/airplane_salt_n_pepper_1_gaussian_blur_1.jpg)

![snp1gb5](Exercises 1/airplane_salt_n_pepper_1_gaussian_blur_5.jpg)

![snp1gb9](Exercises 1/airplane_salt_n_pepper_1_gaussian_blur_9.jpg)

![snp10gb1](Exercises 1/airplane_salt_n_pepper_10_gaussian_blur_1.jpg)

![snp10gb5](Exercises 1/airplane_salt_n_pepper_10_gaussian_blur_5.jpg)

![snp10gb9](Exercises 1/airplane_salt_n_pepper_10_gaussian_blur_9.jpg)

#### Vlastiti median filter

![snp10](Exercises 1/noise_test.jpg)

![mymed3](Exercises 1/my_filter.jpg)

![ormed3](Exercises 1/original_filter.jpg)

## Exercises 2. - thresholding

***Kod***

```python
import numpy as np
import cv2
from matplotlib import pyplot as plt

boats = cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/boats.bmp', 0)
baboon = cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/baboon.bmp', 0)
airplane = cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/airplane.bmp', 0)

images = [boats, baboon, airplane]
img_names = ['boats', 'baboon', 'airplane']

for i in range(len(images)):
    ret, thresh1 = cv2.threshold(images[i], 127, 255, cv2.THRESH_BINARY)
    cv2.imwrite(img_names[i] + '_thresh_binary' + '.jpg', thresh1)
    ret, thresh2 = cv2.threshold(images[i], 127, 255, cv2.THRESH_BINARY_INV)
    cv2.imwrite(img_names[i] + '_thresh_binary_inv' + '.jpg', thresh2)
    ret, thresh3 = cv2.threshold(images[i], 127, 255, cv2.THRESH_TRUNC)
    cv2.imwrite(img_names[i] + '_thresh_trunc' + '.jpg', thresh3)
    ret, thresh4 = cv2.threshold(images[i], 127, 255, cv2.THRESH_TOZERO)
    cv2.imwrite(img_names[i] + '_thresh_tozero' + '.jpg', thresh4)
    ret, thresh5 = cv2.threshold(images[i], 127, 255, cv2.THRESH_TOZERO_INV)
    cv2.imwrite(img_names[i] + '_thresh_tozero_inv' + '.jpg', thresh5)
    th6 = cv2.adaptiveThreshold(images[i], 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(img_names[i] + '_thresh_adaptive_mean' + '.jpg', th6)
    th7 = cv2.adaptiveThreshold(images[i], 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(img_names[i] + '_thresh_adaptive_gaussian' + '.jpg', th7)
    ret, th8 = cv2.threshold(images[i], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    cv2.imwrite(img_names[i] + '_thresh_otsu' + '.jpg', th8)
```

#### Thresh binary

![tb](Exercises 2/boats_thresh_binary.jpg)

#### Thresh binary inv

![tbi](Exercises 2/boats_thresh_binary_inv.jpg)

#### Thresh trunc

![ttr](Exercises 2/boats_thresh_trunc.jpg)

#### Thresh tozero

![tto](Exercises 2/boats_thresh_tozero.jpg)

#### Thresh tozero inv

![tti](Exercises 2/boats_thresh_tozero_inv.jpg)

#### Thresh adaptive mean

![tam](Exercises 2/boats_thresh_adaptive_mean.jpg)

#### Thresh adaptive gaussian

![tag](Exercises 2/boats_thresh_adaptive_gaussian.jpg)

#### Thresh otsu

![to](Exercises 2/boats_thresh_otsu.jpg)

U sklopu vježbe, kao što se može vidjeti iz koda, napravljeno je i za ostale slike, ali radi velikog broja slika nije uključeno u md file.








