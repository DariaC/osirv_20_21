# Compare the above thresholding methods on images boats, baboon
# and airplane. Try using different thresholds for images.

import numpy as np
import cv2
from matplotlib import pyplot as plt

boats = cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/boats.bmp', 0)
baboon = cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/baboon.bmp', 0)
airplane = cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/airplane.bmp', 0)

images = [boats, baboon, airplane]
img_names = ['boats', 'baboon', 'airplane']

for i in range(len(images)):
    ret, thresh1 = cv2.threshold(images[i], 127, 255, cv2.THRESH_BINARY)
    cv2.imwrite(img_names[i] + '_thresh_binary' + '.jpg', thresh1)
    ret, thresh2 = cv2.threshold(images[i], 127, 255, cv2.THRESH_BINARY_INV)
    cv2.imwrite(img_names[i] + '_thresh_binary_inv' + '.jpg', thresh2)
    ret, thresh3 = cv2.threshold(images[i], 127, 255, cv2.THRESH_TRUNC)
    cv2.imwrite(img_names[i] + '_thresh_trunc' + '.jpg', thresh3)
    ret, thresh4 = cv2.threshold(images[i], 127, 255, cv2.THRESH_TOZERO)
    cv2.imwrite(img_names[i] + '_thresh_tozero' + '.jpg', thresh4)
    ret, thresh5 = cv2.threshold(images[i], 127, 255, cv2.THRESH_TOZERO_INV)
    cv2.imwrite(img_names[i] + '_thresh_tozero_inv' + '.jpg', thresh5)
    th6 = cv2.adaptiveThreshold(images[i], 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(img_names[i] + '_thresh_adaptive_mean' + '.jpg', th6)
    th7 = cv2.adaptiveThreshold(images[i], 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,11,2)
    cv2.imwrite(img_names[i] + '_thresh_adaptive_gaussian' + '.jpg', th7)
    ret, th8 = cv2.threshold(images[i], 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    cv2.imwrite(img_names[i] + '_thresh_otsu' + '.jpg', th8)
    