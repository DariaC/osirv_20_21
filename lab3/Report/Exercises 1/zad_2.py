# 1.Corrupt images boats and airplane with salt and pepper noise with
# percent=[1%,10%] percent and gaussian noise with σ=[5,15,35].
# 2.Using the corrupted images explore  the effect of median filtering with
# different neighborhood sizes.
# 3.Filter the corrupted images with median filter and gaussian blur filter.
# Try to determine which filter is better for removal of which type of noise.
# Use various parameters for filters.
# 4.Implement the median filter yourself. Median filter
# function should take two parameters: image to be filtered and radius of median
# filter. Filtering kernel should be a square. Test your implementation against
# openCV implementation. Double points for using C code in Weave (similar to
# convolution example).

import numpy as np
import cv2
import matplotlib.pyplot as plt

img1=cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/boats.bmp')
img2=cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/airplane.bmp')

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

salt_n_pepper_p =[1,10]

gaussian_n=[5, 15, 35]

median=[3, 5, 7]

blur=[1, 5, 9]

for x in gaussian_n:
    img_b = gaussian_noise(img1, 0, x)
    img_a = gaussian_noise(img2, 0, x)
    for y in median:
        img_med_b= cv2.medianBlur(img_b, y)
        cv2.imwrite('boats_gaussian_' + str(x) + '_median_' + str(y) + '.jpg', img_med_b)
        img_med_a= cv2.medianBlur(img_a, y)
        cv2.imwrite('airplane_gaussian_' + str(x) + '_median_' + str(y) + '.jpg', img_med_a)
    for z in blur:
        img_blur_b = cv2.GaussianBlur(img_b, (z, z), x)
        cv2.imwrite('boats_gaussian_'+ str(x) + '_gaussian_blur_' + str(z) + '.jpg', img_blur_b)
        img_blur_a = cv2.GaussianBlur(img_a, (z, z), x)
        cv2.imwrite('airplane_gaussian_'+ str(x) + '_gaussian_blur_' + str(z) + '.jpg', img_blur_a)

for x in salt_n_pepper_p:
    img_b = salt_n_pepper_noise(img1, x)
    img_a = salt_n_pepper_noise(img2, x)
    for y in median:
        img_med_b= cv2.medianBlur(img_b, y)
        cv2.imwrite('boats_salt_n_pepper_' + str(x) + '_median_' + str(y) + '.jpg', img_med_b)
        img_med_a= cv2.medianBlur(img_a, y)
        cv2.imwrite('airplane_salt_n_pepper_' + str(x) + '_median_' + str(y) + '.jpg', img_med_a)
    for z in blur:
        img_blur_b = cv2.GaussianBlur(img_b, (z, z), x)
        cv2.imwrite('boats_salt_n_pepper_'+ str(x) + '_gaussian_blur_' + str(z) + '.jpg', img_blur_b)
        img_blur_a = cv2.GaussianBlur(img_a, (z, z), x)
        cv2.imwrite('airplane_salt_n_pepper_'+ str(x) + '_gaussian_blur_' + str(z) + '.jpg', img_blur_a)

def med_filter(image, radius):
    imgcopy=image
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            imgcopy[i][j]=np.median(imgcopy[i:i+radius, j:j+radius])
    return imgcopy
   
img_z4=cv2.imread('C:/Users/daria/osirv_20_21/lab3/slike/boats.bmp',0)

noise = salt_n_pepper_noise(img_z4, 10)
my_med = med_filter(img_z4, 3)
or_med = cv2.medianBlur(img_z4, 3)

cv2.imwrite('noise_test.jpg', noise)
cv2.imwrite('my_filter.jpg', my_med)
cv2.imwrite('original_filter.jpg', or_med)