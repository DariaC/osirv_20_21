# Izvještaj LV5

 U izvještaju rješenja su prikazana za 3 taska. Exercise 1, 2 i ostalo gdje se samo testira kod, neće biti prikazani.

## Zadatak 1.

***Kod***

```python
import numpy as np
import cv2

cap = cv2.VideoCapture('../road.mp4')

fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi', fourcc, 20.0, (640,480))

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
        
        #zakretanje po x i y osi
        frame = cv2.flip(frame, -1)
        
        out.write(frame)
        cv2.imshow('Frame', frame)
        k = cv2.waitKey(30) & 0xff
        if k == 27:
             break

cap.release()
cv2.destroyAllWindows()
```
 Orginalni kod već zakreće sliku zbog drugog parametra cv2.flip koji je 1 (zakretanje po y osi). 
 Ovdje sam stavila -1 koji zakreće po x i y osi. Parametar iznosa 0 zakreće po x osi.
 Rezultat se prikazuje u obliku slike radi jednostavnosti, kao i rezultati drugih zadataka.

![task1img](task1_img.jpg)

## Zadatak 2.

***Kod***

```python
import cv2 
import numpy as np

#Lucas-Kanade method
cap = cv2.VideoCapture('../road.mp4')
feature_params = dict( maxCorners = 15,
                        qualityLevel = 0.3,
                        minDistance = 7,
                        blockSize = 7 )

lk_params = dict( winSize  = (15,15),
                  maxLevel = 2,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

color = np.random.randint(0,255,(100,3))

ret, old_frame = cap.read()
old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
p0 = cv2.goodFeaturesToTrack(old_gray, mask = None, **feature_params)

mask = np.zeros_like(old_frame)

while(1):
    ret,frame = cap.read()
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)

    good_new = p1[st==1]
    good_old = p0[st==1]

    for i,(new,old) in enumerate(zip(good_new,good_old)):
        a,b = new.ravel()
        c,d = old.ravel()
        mask = cv2.line(mask, (a,b),(c,d), color[i].tolist(), 2)
        frame = cv2.circle(frame,(a,b),5,color[i].tolist(),-1)
    img = cv2.add(frame,mask)
   
    cv2.imshow('frame',img)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
    
    old_gray = frame_gray.copy()
    p0 = good_new.reshape(-1,1,2)
    
    #zatvara video 
    if cv2.getWindowProperty('frame', cv2.WND_PROP_VISIBLE) < 1:
        break
cap.release()

#Dense optical flow 
cap = cv2.VideoCapture("../road.mp4")
ret, frame1 = cap.read()
prvs = cv2.cvtColor(frame1,cv2.COLOR_BGR2GRAY)
hsv = np.zeros_like(frame1)
hsv[...,1] = 255

while(1):
    ret, frame2 = cap.read()
    next = cv2.cvtColor(frame2,cv2.COLOR_BGR2GRAY)
    flow = cv2.calcOpticalFlowFarneback(prvs,next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
    mag, ang = cv2.cartToPolar(flow[...,0], flow[...,1])
    hsv[...,0] = ang*180/np.pi/2
    hsv[...,2] = cv2.normalize(mag,None,0,255,cv2.NORM_MINMAX)
    bgr = cv2.cvtColor(hsv,cv2.COLOR_HSV2BGR)
    cv2.imshow('frame2',bgr)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
    elif k == ord('s'):
        cv2.imwrite('opticalfb.png',frame2)
        cv2.imwrite('opticalhsv.png',bgr)
    prvs = next
    if cv2.getWindowProperty('frame2', cv2.WND_PROP_VISIBLE) < 1:
        break
cap.release()
cv2.destroyAllWindows()
```
 U ovom zadatku se uspoređuju sparse i dense optical flow metode. U sparse se samo određeni pixeli prate, dok
 u dense optical flow svi. Dense optical flow je zbog toga sporiji, ali precizniji.
 Nakon što se zatvori jedan video otvori se drugi, a može se zatvoriti i prije završetka zbog dodanog koda.

![task2img1](task2_img1.jpg)

![task2img2](task2_img2.jpg)

## Zadatak 3.

***Kod***

```python
import cv2


cap = cv2.VideoCapture(0)

if (cap.isOpened()== False):
  print("Error")

while(cap.isOpened()):
  ret, frame = cap.read()
  if ret == True:

    cv2.imshow('Camera frame',frame)
    
    k = cv2.waitKey(1) 
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()
```
![task3img](task3_img.jpg)