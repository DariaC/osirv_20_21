import cv2


cap = cv2.VideoCapture(0)

if (cap.isOpened()== False):
  print("Error")

while(cap.isOpened()):
  ret, frame = cap.read()
  if ret == True:

    cv2.imshow('Camera frame',frame)
    
    k = cv2.waitKey(1) 
    if k == 27:
        break

cap.release()
cv2.destroyAllWindows()